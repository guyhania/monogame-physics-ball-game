﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XELibrary;
using FarseerPhysics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using FarseerPhysics.Common;
using FarseerPhysics.DebugView;

namespace Game13
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
   
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        //Graphics
        private static GraphicsDevice _graphics;
        GraphicsDeviceManager graphics;

        SpriteBatch _batch;

        //Physics
        private World world;
        private const float G = 9.81f;
        private Vector2 gravity = new Vector2(0.0f,G);

        //Controls     
        private KeyboardState _oldKeyState;
        private GamePadState  _oldPadState;

        //Simple camera controls
        private Matrix _view;

        //Camera
        private Camera2D camera;

        //Textures
        private Texture2D _circleSprite,_groundSprite,_IcyGroundSprite,_IcyGroundSprite2,
                          _iceCubeSprite,_btnSprite,_trophySprite,_blockSprite;

        //Body
        private Body circleBody, groundBody, iceCubeBody, btnBody, trophyBody, BlockBody;

       //Position
        private Vector2 _cameraPosition, _screenCenter, _groundOrigin, _groundOrigin2, _groundOrigin3,
                        _circleOrigin,_IcyGroundOrigin, _iceCubeOrigin, _btnOrigin, _trophyOrigin, _blockOrigin;
              
      //bool 
        bool hitBtn = false;
        bool winGame = false;

       //Text
        private SpriteFont gameFont;
        private String findPos;
        private Vector2 TextPos = new Vector2(50, 900);
        private String victory = "You Win";
        private String instructions= "Push the Ice Cube to the Blue BTN to open the door";


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);

            //Screen resolution
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 960;

            Content.RootDirectory = "Content";

            _graphics = GraphicsDevice;

            world = new World(gravity);
                          
        }

       
        protected override void Initialize()
        {        
            base.Initialize();
        }

        protected override void LoadContent()
        {
            camera = new Camera2D(GraphicsDevice);
            gameFont = Content.Load<SpriteFont>("Font");
            Vector2 gameWorld = new Vector2(1280, 960);
            

            _batch = new SpriteBatch(GraphicsDevice);
         
            //Load view properties
            _view = Matrix.Identity;
            _cameraPosition = Vector2.Zero;
            _screenCenter = new Vector2(graphics.GraphicsDevice.Viewport.Width / 2f, graphics.GraphicsDevice.Viewport.Height / 2f);

            // Load game sprites
            _circleSprite = Content.Load<Texture2D>("CircleSprite"); //  96px x 96px => 1.5m x 1.5m
            _groundSprite = Content.Load<Texture2D>("GroundSprite"); // 512px x 64px =>   8m x 1m
            _IcyGroundSprite = Content.Load<Texture2D>("IcyGroundSprite2");
            _IcyGroundSprite2 = Content.Load<Texture2D>("IcyGroundSprite");
            _iceCubeSprite = Content.Load<Texture2D>("IceCube");
            _btnSprite = Content.Load<Texture2D>("btn");
            _trophySprite = Content.Load<Texture2D>("trophy");
            _blockSprite = Content.Load<Texture2D>("Block");

            /* We need monoGame to draw the fixtures at the center of the shapes */
            _groundOrigin = new Vector2(_groundSprite.Width / 2f, _groundSprite.Height / 2f);
            _circleOrigin = new Vector2(_circleSprite.Width / 2f, _circleSprite.Height / 2f);
            _IcyGroundOrigin = new Vector2(_IcyGroundSprite.Width / 2f, (_IcyGroundSprite.Height / 2f));
            _iceCubeOrigin = new Vector2(_iceCubeSprite.Width / 2, _iceCubeSprite.Height / 2);
            _btnOrigin = new Vector2(_btnSprite.Width / 2, _btnSprite.Height / 2);
            _trophyOrigin = new Vector2(_trophySprite.Width / 2, _trophySprite.Height / 2);
            _blockOrigin = new Vector2(_blockSprite.Width / 2, _blockSprite.Height / 2);
            

            // Velcro Physics expects objects to be scaled to MKS (meters, kilos, seconds)
            // 1 meters equals 64 pixels here
            ConvertUnits.SetDisplayUnitToSimUnitRatio(64f);

            /* Circle */
            // Convert screen center from pixels to meters
            Vector2 circlePosition = ConvertUnits.ToSimUnits(_screenCenter) + new Vector2(-6, -2.5f);
            Vector2 iceCubePosition = ConvertUnits.ToSimUnits(_screenCenter) + new Vector2(0,4.5f);
            Vector2 btnPosition = ConvertUnits.ToSimUnits(_screenCenter) + new Vector2(8.75f, 6.5f);
            

            // Create the circle fixture
            circleBody = BodyFactory.CreateCircle(world, ConvertUnits.ToSimUnits(96 / 2f), 1f, circlePosition, BodyType.Dynamic);
            circleBody.CollidesWith = Category.All;
            circleBody.CollisionCategories = Category.All;

            // Give it some bounce and friction
            circleBody.Restitution = 0.3f;
            circleBody.Friction = 0.5f;

            //Create the ice cube fixture
            iceCubeBody = BodyFactory.CreateRectangle(world, ConvertUnits.ToSimUnits(141/2f), ConvertUnits.ToSimUnits(156/2f), 2f,iceCubePosition,0f, BodyType.Dynamic);
            iceCubeBody.Restitution = 0.5f;
            iceCubeBody.Friction = 0.5f;
            iceCubeBody.CollisionCategories = Category.Cat1;
            iceCubeBody.CollidesWith = Category.Cat2;
          

            //Create the btn
            btnBody = BodyFactory.CreateRectangle(world, ConvertUnits.ToSimUnits(140/2), 1f, 0f, btnPosition, 0f, BodyType.Static);
            btnBody.CollisionCategories = Category.Cat2;
            btnBody.CollidesWith = Category.Cat1;

            //Create Block
            Vector2 blockPosition = ConvertUnits.ToSimUnits(_screenCenter) - new Vector2(-7f, 6.3f);
            BlockBody = BodyFactory.CreateRectangle(world, ConvertUnits.ToSimUnits(64f), ConvertUnits.ToSimUnits(244f), 1f, blockPosition);


            /* Ground */
            Vector2 groundPosition = ConvertUnits.ToSimUnits(_screenCenter) + new Vector2(6, 4f);
            Vector2 groundPosition2 = ConvertUnits.ToSimUnits(_screenCenter) - new Vector2(4, 3.0f);
            Vector2 groundPosition3 = ConvertUnits.ToSimUnits(_screenCenter) -new Vector2(-6, 4f);
            Vector2 icyGroundPosition = ConvertUnits.ToSimUnits(_screenCenter) - new Vector2(0, 5.5f);
            Vector2 icyGroundPosition2 = ConvertUnits.ToSimUnits(_screenCenter) - new Vector2(0, 5.5f);
            findPos =icyGroundPosition2.ToString();
            float x= ConvertUnits.ToSimUnits(1280f);
            float y = ConvertUnits.ToSimUnits(960f);
           


            // Create the ground fixture
            groundBody = BodyFactory.CreateRectangle(world, ConvertUnits.ToSimUnits(512f), ConvertUnits.ToSimUnits(64f), 1f, groundPosition);
            groundBody.BodyType = BodyType.Static;
            groundBody.Restitution = 0.2f;
            groundBody.Friction = 0.2f;

            //Creata the Trophy fixture
            Vector2 trophyPos = ConvertUnits.ToSimUnits(_screenCenter) - new Vector2(-9f, 5.1f);
            trophyBody = BodyFactory.CreateRectangle(world, ConvertUnits.ToSimUnits(103), ConvertUnits.ToSimUnits(80), 1f, trophyPos);

            //
            Floor second = new Floor(groundPosition2,world);
            _groundOrigin2 = second.getLocation();

            Floor third = new Floor(groundPosition3, world);
            _groundOrigin3 = third.getLocation();

            gameBorder bord = new gameBorder(world,1280f,960f);
           
        }

        /*
         *Allows the game to run logic such as updating the world,
         *checking for collisions, gathering input, and playing audio.
         */
        protected override void Update(GameTime gameTime)
        {

            base.Update(gameTime);
            HandleGamePad();
            HandleKeyboard();

            float hit = Vector2.Distance(iceCubeBody.Position, btnBody.Position) -1.5f;
            float winGame = Vector2.Distance(circleBody.Position, trophyBody.Position) - 1.5f;
            if (hit < 0.2)
            {
                hitBtn = true;
                BlockBody.IgnoreCollisionWith(circleBody);
            };
          
            if (winGame < 0.2)
            {
                this.winGame = true;
            };

            //We update the world
            world.Step((float)gameTime.ElapsedGameTime.TotalMilliseconds * 0.001f);
      
        }


        private void HandleGamePad()
        {
            GamePadState padState = GamePad.GetState(0);

            if (padState.IsConnected)
            {
                if (padState.Buttons.Back == ButtonState.Pressed)
                    Exit();

                if (padState.Buttons.A == ButtonState.Pressed && _oldPadState.Buttons.A == ButtonState.Released)
                    circleBody.ApplyLinearImpulse(new Vector2(0, -10));

                circleBody.ApplyForce(padState.ThumbSticks.Left);
                _cameraPosition.X -= padState.ThumbSticks.Right.X;
                _cameraPosition.Y += padState.ThumbSticks.Right.Y;

                _view = Matrix.CreateTranslation(new Vector3(_cameraPosition - _screenCenter, 0f)) * Matrix.CreateTranslation(new Vector3(_screenCenter, 0f));

                _oldPadState = padState;
            }
        }

        private void HandleKeyboard()
        {
            KeyboardState state = Keyboard.GetState();

            // Move camera
            if (state.IsKeyDown(Keys.Left))
                _cameraPosition.X += 1.5f;

            if (state.IsKeyDown(Keys.Right))
                _cameraPosition.X -= 1.5f;

            if (state.IsKeyDown(Keys.Up))
                _cameraPosition.Y += 1.5f;

            if (state.IsKeyDown(Keys.Down))
                _cameraPosition.Y -= 1.5f;

            _view = Matrix.CreateTranslation(new Vector3(_cameraPosition - _screenCenter, 0f)) * Matrix.CreateTranslation(new Vector3(_screenCenter, 0f));

            // We make it possible to rotate the circle body
            if (state.IsKeyDown(Keys.A))
                circleBody.ApplyTorque(-10);

            if (state.IsKeyDown(Keys.D))
                circleBody.ApplyTorque(10);

            if (state.IsKeyDown(Keys.Space) && _oldKeyState.IsKeyUp(Keys.Space))
                circleBody.ApplyLinearImpulse(new Vector2(0, -10));

            if (state.IsKeyDown(Keys.Escape))
                Exit();

            _oldKeyState = state;
        }


        /* This is called when the game should draw itself. */
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            _batch.Begin(SpriteSortMode.Deferred,null,null, null,null,null,_view);

            if (winGame)
            {
                _batch.DrawString(gameFont, victory, new Vector2(50, 100), Color.Black);
            };

            _batch.DrawString(gameFont, instructions, new Vector2(50, 200), Color.Black);
            _batch.Draw(_btnSprite, ConvertUnits.ToDisplayUnits(btnBody.Position), null, Color.White, btnBody.Rotation, _btnOrigin, 1f, SpriteEffects.None, 0f);
            _batch.Draw(_circleSprite, ConvertUnits.ToDisplayUnits(circleBody.Position), null, Color.White, circleBody.Rotation, _circleOrigin, 1f, SpriteEffects.None, 0f);
            _batch.Draw(_iceCubeSprite, ConvertUnits.ToDisplayUnits(iceCubeBody.Position),null, Color.White,iceCubeBody.Rotation,_iceCubeOrigin,1f,SpriteEffects.None,0f);
            _batch.Draw(_groundSprite, ConvertUnits.ToDisplayUnits(groundBody.Position), null, Color.White, 0f, _groundOrigin, 1f, SpriteEffects.None, 0f);
            _batch.Draw(_groundSprite, ConvertUnits.ToDisplayUnits(_groundOrigin2), null, Color.White, 0f, _groundOrigin, 1f, SpriteEffects.None, 0f);
            _batch.Draw(_groundSprite, ConvertUnits.ToDisplayUnits(_groundOrigin3), null, Color.White, 0f, _groundOrigin, 1f, SpriteEffects.None, 0f);
            _batch.Draw(_trophySprite, ConvertUnits.ToDisplayUnits(trophyBody.Position), null, Color.White, 0f, _trophyOrigin, 1f, SpriteEffects.None, 0f);
            _batch.Draw(_blockSprite, ConvertUnits.ToDisplayUnits(BlockBody.Position), null, Color.White, 0f, _blockOrigin, 1f, SpriteEffects.None, 0f);
            _batch.End();

            base.Draw(gameTime);
        }
    }
}
